class GCDLoop
{
    public static void main(String[] args)
    {
        int a,b,q,r,gcd; // enter the variables
        a=Integer.parseInt(args[0]); // enter the first number
        b=Integer.parseInt(args[1]); // enter the second number
        while(b!=0)
        {
            q=b;
            r=a%b; // find the remainder
            b=r;
            a=q;
        }
        gcd=a; // store the final gcd gcd
        System.out.println(gcd); // print the gcd
    }
}