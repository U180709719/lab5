class GCDRec {
    public static void main(String[] args) {
        int a, b, q, r, gcd; // enter the variables
        a = Integer.parseInt(args[0]); // enter the first number
        b = Integer.parseInt(args[1]); // enter the second number
        gcd = GCD(a, b); // store the final gcd value
        System.out.println(gcd); // print the value
    }

    public static int GCD(int m, int n) {
        if (m == 0) // base case
        {
            return n;
        } else {
            return (GCD(n % m, m)); // recursive case
        }
    }
}